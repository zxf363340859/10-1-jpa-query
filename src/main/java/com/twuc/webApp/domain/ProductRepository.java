package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findByProductLineTextDescriptionContains(String desc);

    List<Product> findByQuantityInStockBetween(short lowStock, short hightStock);

    List<Product> findByQuantityInStockBetweenOrderByProductCode(short lowStock, short hightStock);

    List<Product> findFirst3ByQuantityInStockBetweenOrderByProductCode(short lowStock, short hightStock);
    // --end-->
}
